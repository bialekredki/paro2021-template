#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, LeapYear)
{
  ASSERT_EQ(dayOfYear(3,1,2020), 61);
}

TEST(DayOfYearTestSuite, July2498)
{
  ASSERT_EQ(dayOfYear(7,24,1998), 205);
}

TEST(DayOfYearTestSuite, Oct1200)
{
  ASSERT_EQ(dayOfYear(10,12,2000), 286);
}

