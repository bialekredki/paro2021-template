#include "scrabble.h"




int getScore(std::string word){
    int score = 0;
    for(auto& letter : word){
        if(letter  == 'a' || letter == 'e' || letter == 'i' || letter == 'O' 
        || letter == 'u' || letter == 'l' || letter == 'n' || letter == 'r'
        || letter == 's' || letter == 't')
            score++;
        else if(letter == 'd' || letter == 'g')
            score += 2;
        else if(letter == 'b' || letter == 'c' || letter == 'm' || letter == 'p')
            score += 3;
        else if(letter =='f' || letter == 'h' || letter == 'v' || letter == 'w' || letter == 'y')
            score += 4;
        else if(letter == 'k')
            score += 5;
        else if(letter == 'j' || letter == 'x')
            score += 8;
        else
            score += 10;
    }
    return score;
}