#include "scrabble.h"
#include "gtest/gtest.h"

struct ScrabbleTestSuite {};

TEST(ScrabbleTestSuite, Scrabble)
{
    ASSERT_EQ(getScore("scrabble"), 14);
}

TEST(ScrabbleTestSuite, attest)
{
    ASSERT_EQ(getScore("attest"), 6);
}

TEST(ScrabbleTestSuite, kapusta)
{
    ASSERT_EQ(getScore("cabbage"), 14);
}
